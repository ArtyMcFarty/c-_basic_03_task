﻿using System;
using System.Diagnostics;

namespace CS_Basic_03_task_2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.Write(ArithmeticProgression(1, 3, 2));
            Console.Write("\n\n");
            Console.Write(GeometricProgression(3, 3, 0.5));
        }

        public static int ArithmeticProgression(int a1, int n, int t)
        {
            int[] ArrayOfNumbers = new int[n];
            int ProductOfNumbers = a1;
            ArrayOfNumbers[0] = a1;

            for (int i = 1; i < n; i++)
            {
                ArrayOfNumbers[i] = ArrayOfNumbers[i - 1] + t;
                ProductOfNumbers = ProductOfNumbers * ArrayOfNumbers[i];
            }

            return ProductOfNumbers;
        }

        public static double GeometricProgression(double a1, int n, double t)
        {
            double[] ArrayOfNumbers = new double[n];
            double ProductOfNumbers = a1;
            ArrayOfNumbers[0] = a1;

            if (t > 0 & t < 1)
            {
                for (int i = 1; i < n; i++)
                {
                    ArrayOfNumbers[i] = ArrayOfNumbers[i - 1] * t;
                    ProductOfNumbers = ProductOfNumbers * ArrayOfNumbers[i];
                }
            }

            return ProductOfNumbers;
        }
    }
}

