﻿using System;
using System.Diagnostics;

namespace CS_Basic_03_task_1
{
    class MainClass
    {
        public enum SortType
        {
            Ascending,
            Descending
        }

        public static void Main(string[] args)
        {
            int[] Array1 = { 1, 3, 2, 4, 5, 6, 7, 8, 9 };

            Debug.Write(ArraySort(Array1, SortType.Ascending));
            ArraySortCheck(Array1, SortType.Ascending);
        }


        public static int[] ArraySort(int[] ArrayToSort, SortType sort)
        {
            int temp;

            if (sort == SortType.Ascending)
            {
                for (int i = 0; i < ArrayToSort.Length - 1; i++)
                {
                    for (int j = i + 1; j < ArrayToSort.Length; j++)
                    {
                        if (ArrayToSort[i] > ArrayToSort[j])
                        {
                            temp = ArrayToSort[i];
                            ArrayToSort[i] = ArrayToSort[j];
                            ArrayToSort[j] = temp;
                        }
                    }
                }
            }
            else if (sort == SortType.Descending)
            {
                for (int i = 0; i < ArrayToSort.Length - 1; i++)
                {
                    for (int j = i + 1; j < ArrayToSort.Length; j++)
                    {
                        if (ArrayToSort[i] < ArrayToSort[j])
                        {
                            temp = ArrayToSort[i];
                            ArrayToSort[i] = ArrayToSort[j];
                            ArrayToSort[j] = temp;
                        }
                    }
                }
            }

            return ArrayToSort;

        }

        public static void ArraySortCheck(int[] ArrayToCheck, SortType sort)
        {
            string answer = "";
            if (sort == SortType.Ascending)
            {
                for (int i = 0; i < ArrayToCheck.Length - 1; i++)
                {
                    if (ArrayToCheck[i] <= ArrayToCheck[i+1])
                    {
                        answer = "Array is sorted!";
                        continue;
                    }
                    else
                    {
                        answer = "Array isn't sorted!";
                        break;
                    }
                }
                Console.Write(answer);
            }
        }

    }
}
